=========================
Password Validation Class
=========================

This class validates a password based upon the
following criteria:

* A password must have at least eight characters.

* A password consists of only letters and digits.

* A password must contain at least three digits.

These parameters can be changed. The user is prompted to enter a password.
The program determines if the criteria has been followed and display results
to the user. This is an updated version, originally written in Java. This
version uses regular expressions.
