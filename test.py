#!/usr/bin/python

# This tests the Password class

from password import Password

print '\nTest Number | Test sample | Result'
print '----------------------------------'

# TEST CONDITIONS
user_input = [
    'test123!@#$',                      # 0 illegal chars
    'test',                             # 1 too short
    'test12323221huiu%$##23h4uiuh3',    # 2 too long
    'test1234',                         # 3 valid
    'testmee1',                         # 4 not enough numbers
    'test  1234  m3',                   # 5 spaces (illegal chars)
    'test-123456',                      # 6 hyphen (illegal chars)
    'test_123456',                      # 7 underscore (illegal chars)
    'TeSt023ME',                        # 8 using caps
    ]

p1 = Password()

# Report all of the test conditions
index = 0
for test in user_input:
    p1.attempt = test
    print index, test, '-->|', p1.password_check(p1.attempt)
    index += 1

print '\n'
