# -----------------------------------------------------------------------------
# password.py
# author: John Merigliano
# created: October 20, 2014
#
# =========================
# Password Validation Class
# =========================
#
# This class validates a password based upon the
# following criteria:
#
# * A password must have at least eight characters.
#
# * A password consists of only letters and digits.
#
# * A password must contain at least three digits.
#
# These paramteters can be changed. The user is prompted to enter a password.
# The program determines if the criteria has been followed and display results
# to the user. This is an updated version, originally written in Java. This
# version uses regular expressions.
# -----------------------------------------------------------------------------

import re


class Password:
    ''' Represents a password object '''

    def __init__(self, user_input=None):
        '''The Password class validates a password.
        The attempt to create a valid password
        is the user_input '''

        self.user_input = user_input

    def get_no_of_letters(self, user_input):
        ''' Returns the amount of letters '''

        # Find all of the letters
        ltrs = re.findall(r'[A-Z,a-z]', user_input)

        no_of_letters = len(ltrs)

        return no_of_letters

    def get_no_of_nos(self, user_input):
        ''' Returns the amount of numbers '''

        # Find all of the numbers
        nos = re.findall(r'[0-9]', user_input)

        no_of_nos = len(nos)

        return no_of_nos

    def password_check(self, user_input):
        ''' Checks a password to ensure valid input,
        as defined below: returns errors or success report '''

        # Specify the minimum length
        min_length = 8

        # Specify the maximum length
        max_length = 18

        # Specify minimum number of numbers
        min_nos = 2

        # Get the amount of letters
        pw_letters = self.get_no_of_letters(user_input)

        # Get the amount of numbers
        pw_numbers = self.get_no_of_nos(user_input)

        # Error codes
        pw_errors = [
            '--valid password--',                           # code 0
            'invalid:--password too short--',               # code 1
            'invalid:--password too long--',                # code 2
            'invalid:--use letters and numbers only--',     # code 3
            'invalid:--need at least 2 numbers--',          # code 4
            ]

        # Reports errors or sucess
        pw_report = []

        # Ensure length
        if len(user_input) < min_length:
            pw_report.append(pw_errors[1])
        elif len(user_input) > max_length:
            pw_report.append(pw_errors[2])
        else:
            # Ensure ltrs and nos only
            if pw_letters + pw_numbers != len(user_input):
                pw_report.append(pw_errors[3])
            elif pw_numbers < min_nos:
                pw_report.append(pw_errors[4])
            else:
                pw_report.append(pw_errors[0])

        return pw_report[0]
